from decimal import Decimal
from enum import Enum
from typing import Literal, Optional
from uuid import UUID, uuid4

import fastapi
import pydantic
from fastapi.applications import FastAPI
from pydantic.color import Color


class Planet(Enum):
    EARTH = "Earth"
    MARS = "Mars"


class Currency(Enum):
    USD = "USD"
    EUR = "EUR"


class Price(pydantic.BaseModel):
    amount: Decimal
    currency: Currency = Currency.EUR


class Poney(pydantic.BaseModel):
    name: str
    sex: Literal["M", "F"]
    color: Optional[Color] = None


class SpaceTripPayload(pydantic.BaseModel):
    departure: Planet
    destination: Planet
    ticket_price: Price
    passengers: list[Poney]


class SpaceTrip(SpaceTripPayload):
    trip_id: UUID


_trips = [
    SpaceTrip(
        trip_id=uuid4(),
        departure=Planet.EARTH,
        destination=Planet.MARS,
        ticket_price=Price(amount=Decimal("9999.99")),
        passengers=[
            Poney(name="Albert", sex="M"),
            Poney(name="Rebecca", sex="F", color=Color("pink")),
        ],
    ),
    SpaceTrip(
        trip_id=uuid4(),
        departure=Planet.MARS,
        destination=Planet.EARTH,
        ticket_price=Price(amount=Decimal("9999.99")),
        passengers=[
            Poney(name="Albert", sex="M"),
            Poney(name="Rebecca", sex="F", color=Color("pink")),
        ],
    ),
]

api = FastAPI()


@api.get("/trips", response_model=list[SpaceTrip])
async def trips(departure: Planet | None = None) -> list[SpaceTrip]:
    """Get all trips"""
    if departure is None:
        return _trips
    return [trip for trip in _trips if trip.departure is departure]


@api.get("/trips/{trip_id}", response_model=SpaceTrip)
async def trip(trip_id: UUID) -> SpaceTrip:
    """Get trip by id"""
    try:
        return next(trip for trip in _trips if trip.trip_id == trip_id)
    except StopIteration as trip_not_found:
        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND
        ) from trip_not_found


@api.post("/trips", response_model=UUID, status_code=fastapi.status.HTTP_201_CREATED)
async def create_trip(payload: SpaceTripPayload) -> UUID:
    """Create a trip"""
    new_trip = SpaceTrip(
        trip_id=uuid4(),
        departure=payload.departure,
        destination=payload.destination,
        passengers=payload.passengers,
        ticket_price=payload.ticket_price,
    )
    _trips.append(new_trip)
    return new_trip.trip_id
