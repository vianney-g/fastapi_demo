import enum
import uuid
from decimal import Decimal

import fastapi
import pydantic

app = fastapi.FastAPI()


class OrderStatus(enum.Enum):
    NEW = "new"
    CANCELED = "canceled"
    SHIPPED = "shipped"


class Price(pydantic.BaseModel):
    currency: str
    amount: Decimal


class Line(pydantic.BaseModel):
    product_id: int
    product_name: str
    quantity: int
    price: Price


class Order(pydantic.BaseModel):
    uuid: uuid.UUID
    status: OrderStatus
    lines: list[Line]


orders_db = [
    Order(
        uuid=uuid.uuid4(),
        status=OrderStatus.NEW,
        lines=[
            Line(
                product_id=12,
                product_name="umbrella",
                quantity=2,
                price=Price(currency="EUR", amount=Decimal("13.37")),
            ),
        ],
    ),
    Order(
        uuid=uuid.uuid4(),
        status=OrderStatus.SHIPPED,
        lines=[
            Line(
                product_id=12,
                product_name="umbrella",
                quantity=1,
                price=Price(currency="EUR", amount=Decimal("13.37")),
            ),
            Line(
                product_id=13,
                product_name="pony",
                quantity=3,
                price=Price(currency="EUR", amount=Decimal("1337.42")),
            ),
        ],
    ),
]


@app.get("/orders", response_model=list[Order])
async def all_orders(status: OrderStatus | None = None) -> list[Order]:
    """Get all orders"""

    if status:
        return [order for order in orders_db if order.status == status]

    return orders_db


@app.get("/orders/{order_uuid}", response_model=Order)
async def get_order(order_uuid: uuid.UUID) -> Order:
    """Get an order by id"""
    try:
        return next(order for order in orders_db if order.uuid == order_uuid)
    except StopIteration as order_not_found:

        raise fastapi.HTTPException(
            status_code=fastapi.status.HTTP_404_NOT_FOUND, detail="Unknown order"
        ) from order_not_found
